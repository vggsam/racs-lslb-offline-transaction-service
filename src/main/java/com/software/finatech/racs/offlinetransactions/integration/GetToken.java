package com.software.finatech.racs.offlinetransactions.integration;

import com.software.finatech.racs.offlinetransactions.dto.SSOToken;
import io.advantageous.boon.json.JsonFactory;
import io.advantageous.boon.json.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@EnableScheduling
@Component
public class GetToken {
    @Value("${sso.baseIdentityURL}")
    private String baseIdentityURL;
    @Value("${sso.apiUsername}")
    private String apiUsername;
    @Value("${sso.apiPassword}")
    private String apiPassword;
    protected ObjectMapper mapper;

    @PostConstruct
    public void initialize(){
        mapper =  JsonFactory.createUseAnnotations(true);
        TokenStoreUtil.TOKEN = getAPIToken();

    }


    @Scheduled(fixedRateString = "3000000")
    public void getAutoToken(){
        TokenStoreUtil.TOKEN = getAPIToken();
    }

    public String getAPIToken(){

        try {
            HttpClient httpclient = HttpClientBuilder.create().build();
            //URI urlObject = new URI(baseIdentityURL+"/connect/token");
            String url = baseIdentityURL+"/connect/token";
            HttpPost httpPost = new HttpPost(url);
            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
            urlParameters.add(new BasicNameValuePair("scope", "identity-server-api"));

            httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
            httpPost.addHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString((apiUsername + ":" + apiPassword).getBytes()));
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");


            HttpResponse response = httpclient.execute(httpPost);
            int responseCode = response.getStatusLine().getStatusCode();
            switch (responseCode) {
                case 200: {
                    // everything is fine, handle the response
                    String stringResponse = EntityUtils.toString(response.getEntity());
                    SSOToken token = mapper.readValue(stringResponse,SSOToken.class);
                    return "Bearer "+token.getAccess_token();
                }
                case 500: {
                    // server problems ?
                    return null;
                }
                case 403: {
                    // you have no authorization to access that resource
                    return null;
                }
            }
        }catch(Throwable e){
            e.printStackTrace();
        }

        return null;
    }
}
