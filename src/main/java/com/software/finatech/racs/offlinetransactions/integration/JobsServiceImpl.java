package com.software.finatech.racs.offlinetransactions.integration;

import com.software.finatech.racs.offlinetransactions.domain.OfflineProcessingTask;
import com.software.finatech.racs.offlinetransactions.dto.*;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;
import com.software.finatech.racs.offlinetransactions.util.ScheduleTaskLogStatusReferenceData;
import io.advantageous.boon.json.JsonFactory;
import io.advantageous.boon.json.ObjectMapper;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class JobsServiceImpl implements JobsService {
    @Autowired
    RestTemplate restTemplate;
    private static Logger logger = LoggerFactory.getLogger(JobsServiceImpl.class);
    @Autowired
    protected MongoRepositoryReactiveImpl mongoRepositoryReactive;



    //@Autowired
    //private Source schedulerChannel;
    protected ObjectMapper mapper;


    @Autowired
    private S3ClientService amazonS3ClientService;

    @PostConstruct
    public void initialize() {
        mapper = JsonFactory.createUseAnnotations(true);
    }

    @Override
    @Scheduled(fixedRate =100000)
    public Boolean scheduleJobs() {
        logger.info("Jobs Scheduler - scheduleJobs...Starts");
        Query query = new Query();
        query.addCriteria(Criteria.where("processed").is(false));
        query.addCriteria(Criteria.where("uploadSuccess").is(true));
        query.limit(10);
        ArrayList<OfflineProcessingTask> tasks = (ArrayList<OfflineProcessingTask>) mongoRepositoryReactive.findAll(query, OfflineProcessingTask.class).toStream().collect(Collectors.toList());

        tasks.parallelStream().forEach(task -> {
            task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.RUNNING);
            task.setProcessed(true);
            task.setProcessingStartDateTime(LocalDateTime.now());
            mongoRepositoryReactive.saveOrUpdate(task);
            pullGamesData(task);
        });

        logger.info("Jobs Scheduler - scheduleJobs...End");
        return true;
    }


    /**
     * Performs the actual Http request
     *
     * @param task
     */
    protected void pullGamesData(OfflineProcessingTask task) {
          try {
            s3BucketPull(task);
        } catch (Throwable ex) {
              logger.info("Loading "+ task.getOriginalFileName() + " Failed: ", ex.getMessage());
            ex.printStackTrace();
            task.setProcessingEndDateTime(LocalDateTime.now());
            task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
            task.setStopOnError(true);
            task.setErrorMessage(ex.getMessage());
            mongoRepositoryReactive.saveOrUpdate(task);
        }
    }


    public void s3BucketPull(OfflineProcessingTask task) {
      this.amazonS3ClientService.readFileFromS3Bucket(task.getS3BucketFileName(), task);
       }


}
