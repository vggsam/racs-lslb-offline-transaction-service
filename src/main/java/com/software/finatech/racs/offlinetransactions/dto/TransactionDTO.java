package com.software.finatech.racs.offlinetransactions.dto;

public class TransactionDTO {
    protected String id;
    protected String institutionId;
    protected String transactionDate;
    protected String transactionTime;
    protected String transactionDateTime;
    protected String gameType;
    protected Double amountStaked;
    protected String gameName;
    protected Double longitudeLocation;
    protected Double latitudeLocation;
    protected String status;
    protected String modeOfPlay;
    protected String institutionName;
    protected Double royaltyFee;
    protected Double gamingTax;
    protected String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getRoyaltyFee() {
        return royaltyFee;
    }

    public void setRoyaltyFee(Double royaltyFee) {
        this.royaltyFee = royaltyFee;
    }

    public Double getGamingTax() {
        return gamingTax;
    }

    public void setGamingTax(Double gamingTax) {
        this.gamingTax = gamingTax;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getModeOfPlay() {
        return modeOfPlay;
    }

    public void setModeOfPlay(String modeOfPlay) {
        this.modeOfPlay = modeOfPlay;
    }
    public Double getLongitudeLocation() {
        return longitudeLocation;
    }

    public void setLongitudeLocation(Double longitudeLocation) {
        this.longitudeLocation = longitudeLocation;
    }

    public Double getLatitudeLocation() {
        return latitudeLocation;
    }

    public void setLatitudeLocation(Double latitudeLocation) {
        this.latitudeLocation = latitudeLocation;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public Double getAmountStaked() {
        return amountStaked;
    }

    public void setAmountStaked(Double amountStaked) {
        this.amountStaked = amountStaked;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getInstitutionId() {
        return institutionId;
    }
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getStatus() {
        return status;
    }
}
