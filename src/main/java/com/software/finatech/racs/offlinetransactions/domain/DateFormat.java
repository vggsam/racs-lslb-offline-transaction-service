package com.software.finatech.racs.offlinetransactions.domain;

import com.software.finatech.racs.offlinetransactions.dto.DateFormatDto;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DateFormats")
public class DateFormat extends EnumeratedFact {

    public DateFormatDto convertToDto() {
        DateFormatDto dateFormatDto = new DateFormatDto();
        dateFormatDto.setCode(getCode());
        dateFormatDto.setDescription(getDescription());
        dateFormatDto.setName(getName());
        dateFormatDto.setId(getId());

        return dateFormatDto;
    }
}
