package com.software.finatech.racs.offlinetransactions.integration;

import com.software.finatech.racs.offlinetransactions.domain.OfflineProcessingTask;


public interface S3ClientService {

        void readFileFromS3Bucket(String s3BucketName, OfflineProcessingTask task);

}
