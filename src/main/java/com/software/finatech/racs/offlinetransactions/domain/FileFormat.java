package com.software.finatech.racs.offlinetransactions.domain;

import com.software.finatech.racs.offlinetransactions.dto.FileFormatDto;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FileFormats")
public class FileFormat extends EnumeratedFact {

    public FileFormatDto convertToDto() {
        FileFormatDto fileFormatDto = new FileFormatDto();
        fileFormatDto.setCode(getCode());
        fileFormatDto.setDescription(getDescription());
        fileFormatDto.setName(getName());
        fileFormatDto.setId(getId());

        return fileFormatDto;
    }
}
