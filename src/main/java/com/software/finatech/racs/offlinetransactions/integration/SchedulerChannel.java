package com.software.finatech.racs.offlinetransactions.integration;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface SchedulerChannel {


    String NEW_GAMES = "newGames";
    String GAMES_STATUS_CHANGE = "gamesStatusChange";


    @Output(NEW_GAMES)
    MessageChannel pullNewGames();

    @Output(GAMES_STATUS_CHANGE)
    SubscribableChannel pullGamesStatusChange();
    String OUT_BOUND_PUSH_AUDIT_EVENTS = "auditEvents";

    @Output(OUT_BOUND_PUSH_AUDIT_EVENTS)
    SubscribableChannel dataAuditTransaction();
}
