package com.software.finatech.racs.offlinetransactions.dto;


public class OSBTransactionDto extends TransactionDTO {

    protected String slipId;
    protected String slipType;
    protected Double possibleWin;
    protected Double confirmedWin;
    protected Double bonus;


    public String getSlipType() {
        return slipType;
    }

    public void setSlipType(String slipType) {
        this.slipType = slipType;
    }
    

    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlipId() {
        return slipId;
    }

    public void setSlipId(String slipId) {
        this.slipId = slipId;
    }


    public Double getPossibleWin() {
        return possibleWin;
    }

    public void setPossibleWin(Double possibleWin) {
        this.possibleWin = possibleWin;
    }

    public Double getConfirmedWin() {
        return confirmedWin;
    }

    public void setConfirmedWin(Double confirmedWin) {
        this.confirmedWin = confirmedWin;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public Double getRoyaltyFee() {
        return royaltyFee;
    }

    public void setRoyaltyFee(Double royaltyFee) {
        this.royaltyFee = royaltyFee;
    }

    public Double getGamingTax() {
        return gamingTax;
    }

    public void setGamingTax(Double gamingTax) {
        this.gamingTax = gamingTax;
    }
}