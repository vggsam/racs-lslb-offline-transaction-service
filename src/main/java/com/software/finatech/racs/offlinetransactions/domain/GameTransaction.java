package com.software.finatech.racs.offlinetransactions.domain;


public class GameTransaction {

    protected String id;
    protected String slipType;
    protected double possibleWin;
    protected String slipTypeId;
    protected double bonus;
    protected String terminalId;
    protected String agentId;
    protected String gameName;
    protected String gameId;
    protected double confirmedWin;
    protected double amountStaked;
    protected long transactionDate;
    protected String transactionTime;
    protected long transactionDateTime;
    protected String institutionId;
    protected String gameType;
    protected double longitude;
    protected double latitude;
    protected String modeOfPlayId;
    protected String statusId;
    protected String institutionName;
    protected boolean duplicateGame;
    protected double royaltyFee;
    protected double gamingTax;
    protected long drawDateTime;



    public long getDrawDateTime() {
        return drawDateTime;
    }

    public void setDrawDateTime(long drawDateTime) {
        this.drawDateTime = drawDateTime;
    }
    public double getRoyaltyFee() {
        return royaltyFee;
    }

    public void setRoyaltyFee(double royaltyFee) {
        this.royaltyFee = royaltyFee;
    }

    public double getGamingTax() {
        return gamingTax;
    }

    public void setGamingTax(double gamingTax) {
        this.gamingTax = gamingTax;
    }

    public boolean getDuplicateGame() {
        return duplicateGame;
    }

    public void setDuplicateGame(boolean duplicateGame) {
        this.duplicateGame = duplicateGame;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlipType() {
        return slipType;
    }

    public void setSlipType(String slipType) {
        this.slipType = slipType;
    }

    public double getPossibleWin() {
        return possibleWin;
    }

    public void setPossibleWin(double possibleWin) {
        this.possibleWin = possibleWin;
    }

    public String getSlipTypeId() {
        return slipTypeId;
    }

    public void setSlipTypeId(String slipTypeId) {
        this.slipTypeId = slipTypeId;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public double getConfirmedWin() {
        return confirmedWin;
    }

    public void setConfirmedWin(double confirmedWin) {
        this.confirmedWin = confirmedWin;
    }

    public double getAmountStaked() {
        return amountStaked;
    }

    public void setAmountStaked(double amountStaked) {
        this.amountStaked = amountStaked;
    }

    public long getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(long transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public long getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(long transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getModeOfPlayId() {
        return modeOfPlayId;
    }

    public void setModeOfPlayId(String modeOfPlayId) {
        this.modeOfPlayId = modeOfPlayId;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }


}
