package com.software.finatech.racs.offlinetransactions.dto;

public class OfflineProcessingTaskDto {
    protected String id;
    protected String originalFileName;
    protected String ErrorMessage;
    protected boolean enabled;
    protected boolean processed;
    protected Boolean stopOnError;
    protected String institutionId;
    protected String institutionName;
    protected String uploadStartDateTime;
    protected String uploadEndDateTime;
    protected boolean uploadSuccess;
    protected String processingStartDateTime;
    protected String processingEndDateTime;
    protected String tenantId;
    protected String gameTypeId;
    protected String scheduleTaskLogStatus;
     protected String userId;
    protected String userFullName;
//    protected String approvalStatusName;
//    protected String initiatorId;
//    protected String initiatorAuthRoleId;
//    protected String initiatorName;
//    protected String approvalName;
//    protected String rejectionReason;
//    protected String approvalActionDateTime;
//    protected String approvalRequestTypeName;

//    public String getApprovalStatusName() {
//        return approvalStatusName;
//    }
//
//    public void setApprovalStatusName(String approvalStatusName) {
//        this.approvalStatusName = approvalStatusName;
//    }

    public String getScheduleTaskLogStatus() {
        return scheduleTaskLogStatus;
    }

    public void setScheduleTaskLogStatus(String scheduleTaskLogStatus) {
        this.scheduleTaskLogStatus = scheduleTaskLogStatus;
    }

//    public String getInitiatorId() {
//        return initiatorId;
//    }
//
//    public void setInitiatorId(String initiatorId) {
//        this.initiatorId = initiatorId;
//    }
//
//    public String getInitiatorAuthRoleId() {
//        return initiatorAuthRoleId;
//    }
//
//    public void setInitiatorAuthRoleId(String initiatorAuthRoleId) {
//        this.initiatorAuthRoleId = initiatorAuthRoleId;
//    }
//
//    public String getInitiatorName() {
//        return initiatorName;
//    }
//
//    public void setInitiatorName(String initiatorName) {
//        this.initiatorName = initiatorName;
//    }
//
//    public String getApprovalName() {
//        return approvalName;
//    }
//
//    public void setApprovalName(String approvalName) {
//        this.approvalName = approvalName;
//    }
//
//    public String getRejectionReason() {
//        return rejectionReason;
//    }
//
//    public void setRejectionReason(String rejectionReason) {
//        this.rejectionReason = rejectionReason;
//    }
//
//    public String getApprovalActionDateTime() {
//        return approvalActionDateTime;
//    }
//
//    public void setApprovalActionDateTime(String approvalActionDateTime) {
//        this.approvalActionDateTime = approvalActionDateTime;
//    }
//
//    public String getApprovalRequestTypeName() {
//        return approvalRequestTypeName;
//    }
//
//    public void setApprovalRequestTypeName(String approvalRequestTypeName) {
//        this.approvalRequestTypeName = approvalRequestTypeName;
//    }

//    public boolean isEnabled() {
//        return enabled;
//    }
//
//    public void setEnabled(boolean enabled) {
//        this.enabled = enabled;
//    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(String gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getErrorMessage() {
        return this.ErrorMessage;
    }

    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getStopOnError() {
        return this.stopOnError;
    }

    public void setStopOnError(Boolean stopOnError) {
        this.stopOnError = stopOnError;
    }

//    public String getRanTime() {
//        return ranTime;
//    }
//
//    public void setRanTime(String ranTime) {
//        this.ranTime = ranTime;
//    }

    public String getUploadStartDateTime() {
        return uploadStartDateTime;
    }

    public void setUploadStartDateTime(String uploadStartDateTime) {
        this.uploadStartDateTime = uploadStartDateTime;
    }

    public String getUploadEndDateTime() {
        return uploadEndDateTime;
    }

    public void setUploadEndDateTime(String uploadEndDateTime) {
        this.uploadEndDateTime = uploadEndDateTime;
    }

    public String getProcessingStartDateTime() {
        return processingStartDateTime;
    }

    public void setProcessingStartDateTime(String processingStartDateTime) {
        this.processingStartDateTime = processingStartDateTime;
    }

    public String getProcessingEndDateTime() {
        return processingEndDateTime;
    }

    public void setProcessingEndDateTime(String processingEndDateTime) {
        this.processingEndDateTime = processingEndDateTime;
    }

    public String getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public boolean isUploadSuccess() {
        return uploadSuccess;
    }

    public void setUploadSuccess(boolean uploadSuccess) {
        this.uploadSuccess = uploadSuccess;
    }

}
