package com.software.finatech.racs.offlinetransactions.util;


import com.software.finatech.racs.offlinetransactions.domain.FileFormat;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;


public class FileTypeReferenceData {
    public final static String EXCEL="1";
    public final static String CSV="2";
    public final static String TXT="3";
    public static void load(MongoRepositoryReactiveImpl mongoRepositoryReactive){


        FileFormat file1 = (FileFormat) mongoRepositoryReactive.findById(EXCEL, FileFormat.class).block();
        if(file1==null){
            file1 = new FileFormat();
            file1.setId(EXCEL);

        }
        file1.setDescription("EXCEL");
        file1.setName("EXCEL");

        FileFormat file2 = (FileFormat) mongoRepositoryReactive.findById(CSV, FileFormat.class).block();
        if(file2==null){
            file2 = new FileFormat();
            file2.setId(CSV);

        }
        file2.setDescription("CSV");
        file2.setName("CSV");


        FileFormat file3 = (FileFormat) mongoRepositoryReactive.findById(TXT, FileFormat.class).block();
        if(file3==null){
            file3 = new FileFormat();
            file3.setId(TXT);

        }
        file3.setDescription("TXT");
        file3.setName("TXT");




        mongoRepositoryReactive.saveOrUpdate(file1);
        mongoRepositoryReactive.saveOrUpdate(file2);
        mongoRepositoryReactive.saveOrUpdate(file3);



    }
}
