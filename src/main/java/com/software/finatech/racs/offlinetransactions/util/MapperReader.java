package com.software.finatech.racs.offlinetransactions.util;


import com.software.finatech.racs.offlinetransactions.domain.HeaderMapping;
import com.software.finatech.racs.offlinetransactions.dto.OSBTransactionDto;
import com.software.finatech.racs.offlinetransactions.dto.POLTransactionDto;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

import java.util.Map;

@Component
public class MapperReader {

    @Autowired
    MongoRepositoryReactiveImpl mongoRepositoryReactive;

    private static final Logger logger = LoggerFactory.getLogger(MapperReader.class);


    public Map<String, ColumnIndex> getInitialMappingFromFirstLine(String formattedLine, HeaderMapping headerMapping) throws Exception {
        HashMap<String, ColumnIndex> map = new HashMap<>();

        map.put("gameId", ColumnIndex.fromColumnName(headerMapping.getGameId()));
        map.put("stakedAmount", ColumnIndex.fromColumnName(headerMapping.getStakedAmount()));
        if(!StringUtils.isEmpty(headerMapping.getGameName())){
            map.put("gameName", ColumnIndex.fromColumnName(headerMapping.getGameName()));
        }
        if(!StringUtils.isEmpty(headerMapping.getModeOfPlay())){
            map.put("modeOfPlay", ColumnIndex.fromColumnName(headerMapping.getModeOfPlay()));
        }if(!StringUtils.isEmpty(headerMapping.getTerminalId())){
            map.put("terminalId", ColumnIndex.fromColumnName(headerMapping.getTerminalId()));
        }if(!StringUtils.isEmpty(headerMapping.getTransactionTime())){
            map.put("transactionTime", ColumnIndex.fromColumnName(headerMapping.getTransactionTime()));
        }if(!StringUtils.isEmpty(headerMapping.getTransactionDate())){
            map.put("transactionDate", ColumnIndex.fromColumnName(headerMapping.getTransactionDate()));
        }if(!StringUtils.isEmpty(headerMapping.getTransactionDateTime())){
            map.put("transactionDateTime", ColumnIndex.fromColumnName(headerMapping.getTransactionDateTime()));
        }if(!StringUtils.isEmpty(headerMapping.getBonus())){
            map.put("bonus", ColumnIndex.fromColumnName(headerMapping.getBonus()));
        }if(!StringUtils.isEmpty(headerMapping.getPossibleWin())){
            map.put("possibleWin", ColumnIndex.fromColumnName(headerMapping.getPossibleWin()));
        }if(!StringUtils.isEmpty(headerMapping.getSlipType())){
            map.put("slipType", ColumnIndex.fromColumnName(headerMapping.getSlipType()));
        }if(!StringUtils.isEmpty(headerMapping.getAgentId())){
            map.put("agentId", ColumnIndex.fromColumnName(headerMapping.getAgentId()));
        }if(!StringUtils.isEmpty(headerMapping.getDrawDateTime())){
            map.put("drawDateTime", ColumnIndex.fromColumnName(headerMapping.getDrawDateTime()));
        }if(!StringUtils.isEmpty(headerMapping.getStatus())){
            map.put("status", ColumnIndex.fromColumnName(headerMapping.getStatus()));
        }if(!StringUtils.isEmpty(headerMapping.getConfirmedWin())){
            map.put("confirmedWin", ColumnIndex.fromColumnName(headerMapping.getConfirmedWin()));
        }


        String[] lineSplitted = formattedLine.split(headerMapping.getCsvSplitter());
        for (String key : map.keySet()) {
            int index = getStringIndexInArray(map.get(key).getColumnName(), lineSplitted);
            if (index == -1) {
                throw new Exception(map.get(key).getColumnName()+ " mapper is invalid for this institution ");
            }
            ColumnIndex columnIndex = map.get(key);
            map.put(key, ColumnIndex.fromColumnNameAndNumber(columnIndex.getColumnName(), index));
        }
        return map;
    }


    public OSBTransactionDto getOSBTransactionFromLineAndMap(String formattedLine, Map<String, ColumnIndex> map, HeaderMapping headerMapping) throws Exception {
        OSBTransactionDto osbTransactionDto = new OSBTransactionDto();

        String csvSplitter = headerMapping.getCsvSplitter();
        String[] columns = formattedLine.split(csvSplitter);
        osbTransactionDto.setSlipId(columns[map.get("gameId").getIndexNumber()]);
        osbTransactionDto.setAmountStaked(Double.valueOf(columns[map.get("stakedAmount").getIndexNumber()]));

        if(!StringUtils.isEmpty(headerMapping.getGameName())){
            osbTransactionDto.setGameName(columns[map.get("gameName").getIndexNumber()]);
        }else{
            osbTransactionDto.setGameName("sport");
        }
        if(!StringUtils.isEmpty(headerMapping.getBonus())){
            osbTransactionDto.setBonus(Double.valueOf(columns[map.get("bonus").getIndexNumber()]));
        }if(!StringUtils.isEmpty(headerMapping.getPossibleWin())){
            osbTransactionDto.setPossibleWin(Double.valueOf(columns[map.get("possibleWin").getIndexNumber()]));
        }if(!StringUtils.isEmpty(headerMapping.getStatus())){
            if(StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "Yes") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "Win") ||
                            StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "True")||
                    StringUtils.equals((columns[map.get("status").getIndexNumber()]).toString(), "2")){
                osbTransactionDto.setStatus("01");

            }else if(StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "No") ||
                        StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "Loss") ||
                        StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "False")||
                    (StringUtils.equals(columns[map.get("status").getIndexNumber()].toString(), "1"))){
                osbTransactionDto.setStatus("02");
            }

        }if(!StringUtils.isEmpty(headerMapping.getModeOfPlay())){
            if(StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "mob") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "mobile")){
                osbTransactionDto.setModeOfPlay("02");
            }else if(StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "web") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "online")){
                osbTransactionDto.setModeOfPlay("01");
            }else if(StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "ret") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "retail")){
                osbTransactionDto.setModeOfPlay("03");
            }else{
                osbTransactionDto.setModeOfPlay("04");
            }

        }

        if(!StringUtils.isEmpty(headerMapping.getConfirmedWin())){
            osbTransactionDto.setConfirmedWin(Double.valueOf(columns[map.get("confirmedWin").getIndexNumber()]));
            if(Double.valueOf(columns[map.get("confirmedWin").getIndexNumber()]) > 0.0 ){
                osbTransactionDto.setStatus("01");
            }
        }

        if(!StringUtils.isEmpty(headerMapping.getTransactionDateTime())){
            String[] dateTime = columns[map.get("transactionDate").getIndexNumber()].split(" ");
            String date = setDate(headerMapping, dateTime[0]);
            if(date == null){
                throw new Exception("Invalid dateFormat... Date format does not match");
            }else{
                osbTransactionDto.setTransactionDate(date);
                osbTransactionDto.setTransactionTime(dateTime[1].substring(0, 8));
            }

        }else{
            String date = setDate(headerMapping, columns[map.get("transactionDate").getIndexNumber()]);
            if(date == null){
                throw new Exception("Invalid dateFormat... Date format does not match");
            }else{
                osbTransactionDto.setTransactionDate(date);
                osbTransactionDto.setTransactionTime(columns[map.get("transactionTime").getIndexNumber()].substring(0, 8));
            }
        }
        return osbTransactionDto;
    }

    public POLTransactionDto getPOLTransactionFromLineAndMap(String formattedLine, Map<String, ColumnIndex> map, HeaderMapping headerMapping) throws Exception {
       POLTransactionDto polTransactionDto = new POLTransactionDto();

        String csvSplitter = headerMapping.getCsvSplitter();
        String[] columns = formattedLine.split(csvSplitter);
        polTransactionDto.setTicketId(columns[map.get("gameId").getIndexNumber()]);
        if(!StringUtils.isEmpty(headerMapping.getGameName())){
            polTransactionDto.setGameName(columns[map.get("gameName").getIndexNumber()]);
        }else{
            polTransactionDto.setGameName("lottery");
        }
        polTransactionDto.setAmountStaked(Double.valueOf(columns[map.get("stakedAmount").getIndexNumber()]));
      if(!StringUtils.isEmpty(headerMapping.getStatus())){
            if(StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "Yes") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "Win") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "True")||
                    (StringUtils.equals(columns[map.get("status").getIndexNumber()].toString(), "2"))){
                polTransactionDto.setStatus("01");

            }else if(StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "No") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "Loss") ||
                    StringUtils.equalsIgnoreCase(columns[map.get("status").getIndexNumber()], "False")||
                    (StringUtils.equals(columns[map.get("status").getIndexNumber()].toString(), "1"))){
                polTransactionDto.setStatus("02");
            }
            if(!StringUtils.isEmpty(headerMapping.getConfirmedWin())){
                polTransactionDto.setAmountWon(Double.valueOf(columns[map.get("confirmedWin").getIndexNumber()]));
                polTransactionDto.setStatus("01");
            }
            if(!StringUtils.isEmpty(headerMapping.getModeOfPlay())){
                if(StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "mob") ||
                        StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "mobile")){
                    polTransactionDto.setModeOfPlay("02");
                }else if(StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "web") ||
                        StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "online")){
                    polTransactionDto.setModeOfPlay("01");
                }else if(StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "ret") ||
                        StringUtils.equalsIgnoreCase(columns[map.get("modeOfPlay").getIndexNumber()], "retail")){
                    polTransactionDto.setModeOfPlay("03");
                }else{
                    polTransactionDto.setModeOfPlay("04");
                }
            }
            if(!StringUtils.isEmpty(headerMapping.getTerminalId())){
                polTransactionDto.setTerminalId(columns[map.get("terminalId").getIndexNumber()]);
            }if(!StringUtils.isEmpty(headerMapping.getAgentId())){
                polTransactionDto.setAgentId(columns[map.get("agentId").getIndexNumber()]);
            }if(!StringUtils.isEmpty(headerMapping.getDrawDateTime())){
                polTransactionDto.setDrawDateTime(columns[map.get("drawDateTime").getIndexNumber()]);
            }
          if(!StringUtils.isEmpty(headerMapping.getTransactionDateTime())){
              String[] dateTime = columns[map.get("transactionDateTime").getIndexNumber()].split(" ");
              String date = setDate(headerMapping, dateTime[0]);
              if(date == null){
                  throw new Exception("Invalid dateFormat... Date format does not match");
              }else{
                  polTransactionDto.setTransactionDate(date);
                  polTransactionDto.setTransactionTime(dateTime[1].substring(0, 8));
              }
          }else{
              String date = setDate(headerMapping, columns[map.get("transactionDate").getIndexNumber()]);
              if(date == null){
                  throw new Exception("Invalid dateFormat... Date format does not match");
              }else{
                  polTransactionDto.setTransactionDate(date);
                  polTransactionDto.setTransactionTime(columns[map.get("transactionTime").getIndexNumber()].substring(0, 7));
              }

          }

        }
        return polTransactionDto;
    }

    private int getStringIndexInArray(String s, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (StringUtils.equalsIgnoreCase(s, array[i])) {
                return i;
            }
        }
        return -1;
    }

    private String setDate(HeaderMapping headerMapping, String column){
        String date = null;
        String[] dateArray;
        if(headerMapping.getTransactionDateFormat().equals("TYPE1")){
            dateArray = column.split("/");
            if(dateArray[2].length() != 4){
                return null;
            }
            date = dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0];
        }if(headerMapping.getTransactionDateFormat().equals("TYPE2")){
            dateArray = column.split("-");
            if(dateArray[2].length() != 4){
                return null;
            }
            date = dateArray[2]+"-"+dateArray[1]+"-"+dateArray[0];
        }if(headerMapping.getTransactionDateFormat().equals("TYPE3")){
            dateArray = column.split("-");
            if(dateArray[2].length() != 4){
                return null;
            }
            date = dateArray[2]+"-"+dateArray[0]+"-"+dateArray[1];
        }if(headerMapping.getTransactionDateFormat().equals("TYPE4")){
            dateArray = column.split("/");
            if(dateArray[2].length() != 4){
                return null;
            }
            date = dateArray[2]+"-"+dateArray[0]+"-"+dateArray[1];
        }if(headerMapping.getTransactionDateFormat().equals("TYPE5")){
            if(column.split("-")[0].length() != 4){
                return null;
            }
            date = column;
        }if(headerMapping.getTransactionDateFormat().equals("TYPE6")){
            dateArray = column.split("/");
            if(dateArray[0].length() != 4){
                return null;
            }
            date = dateArray[0]+"-"+dateArray[1]+"-"+dateArray[2];
        }
        return date;
    }
}
