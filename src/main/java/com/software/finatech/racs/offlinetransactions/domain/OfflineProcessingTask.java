package com.software.finatech.racs.offlinetransactions.domain;

import com.software.finatech.racs.offlinetransactions.dto.OfflineProcessingTaskDto;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "OfflineProcessingTasks")
public class OfflineProcessingTask extends AbstractFact{
    protected String userId;
    protected String userFullName;
    protected String originalFileName;
    protected String errorMessage;
    protected String s3BucketFileName;
    //protected boolean enabled;
    protected boolean processed;
    protected boolean stopOnError;
    protected String scheduleTaskLogStatusId;
    protected LocalDateTime uploadStartDateTime;
    protected LocalDateTime uploadEndDateTime;
    protected boolean uploadSuccess;
    protected LocalDateTime processingStartDateTime;
    protected LocalDateTime processingEndDateTime;
    protected String gameTypeId;
    protected String institutionId;
    protected String institutionName;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }
    public String getGameTypeId() {
        return gameTypeId;
    }
    public void setGameTypeId(String gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getScheduleTaskLogStatusId() {
        return scheduleTaskLogStatusId;
    }

    public void setScheduleTaskLogStatusId(String scheduleTaskLogStatusId) {
        this.scheduleTaskLogStatusId = scheduleTaskLogStatusId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }


//    public boolean getEnabled() {
//        return enabled;
//    }
//
//    public void setEnabled(boolean enabled) {
//        this.enabled = enabled;
//    }


    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getS3BucketFileName() {
        return s3BucketFileName;
    }

    public void setS3BucketFileName(String s3BucketFileName) {
        this.s3BucketFileName = s3BucketFileName;
    }

    public boolean getStopOnError() {
        return stopOnError;
    }

    public void setStopOnError(boolean stopOnError) {
        this.stopOnError = stopOnError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public boolean isUploadSuccess() {
        return uploadSuccess;
    }

    public void setUploadSuccess(boolean uploadSuccess) {
        this.uploadSuccess = uploadSuccess;
    }

    public LocalDateTime getUploadStartDateTime() {
        return uploadStartDateTime;
    }

    public void setUploadStartDateTime(LocalDateTime uploadStartDateTime) {
        this.uploadStartDateTime = uploadStartDateTime;
    }


    public LocalDateTime getUploadEndDateTime() {
        return uploadEndDateTime;
    }

    public void setUploadEndDateTime(LocalDateTime uploadEndDateTime) {
        this.uploadEndDateTime = uploadEndDateTime;
    }


    public LocalDateTime getProcessingStartDateTime() {
        return processingStartDateTime;
    }

    public void setProcessingStartDateTime(LocalDateTime processingStartDateTime) {
        this.processingStartDateTime = processingStartDateTime;
    }

    public LocalDateTime getProcessingEndDateTime() {
        return processingEndDateTime;
    }

    public void setProcessingEndDateTime(LocalDateTime processingEndDateTime) {
        this.processingEndDateTime = processingEndDateTime;
    }


    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public boolean isStopOnError() {
        return stopOnError;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    @Override
    public String getFactName() {
        return "OfflineProcessingTask";
    }

    public OfflineProcessingTaskDto convertToDto() {
        OfflineProcessingTaskDto offlineProcessingTaskDto = new OfflineProcessingTaskDto();
        // offlineProcessingTaskDto.setEnabled(getEnabled());
        offlineProcessingTaskDto.setId(getId());
        offlineProcessingTaskDto.setErrorMessage(getErrorMessage());
        offlineProcessingTaskDto.setUploadStartDateTime(getUploadStartDateTime()==null ? null:getUploadStartDateTime().toString("dd-MM-yyyy hh:mm:ss"));
        offlineProcessingTaskDto.setUploadEndDateTime((getUploadEndDateTime()==null?null:getUploadEndDateTime().toString("dd-MM-yyyy hh:mm:ss")));
        offlineProcessingTaskDto.setUploadSuccess(isUploadSuccess());
        offlineProcessingTaskDto.setProcessingStartDateTime(getProcessingStartDateTime()==null ? null:getProcessingStartDateTime().toString("dd-MM-yyyy hh:mm:ss"));
        offlineProcessingTaskDto.setProcessingEndDateTime((getProcessingEndDateTime()==null?null:getProcessingEndDateTime().toString("dd-MM-yyyy hh:mm:ss")));
        offlineProcessingTaskDto.setStopOnError(getStopOnError());
        offlineProcessingTaskDto.setTenantId(getTenantId());
        offlineProcessingTaskDto.setOriginalFileName(getOriginalFileName());
        offlineProcessingTaskDto.setProcessed(isProcessed());
        offlineProcessingTaskDto.setScheduleTaskLogStatus(getScheduleTaskLogStatusId());
        offlineProcessingTaskDto.setGameTypeId(getGameTypeId());
        offlineProcessingTaskDto.setInstitutionName(getInstitutionName());
        if(!StringUtils.isEmpty(getInstitutionId())){
            offlineProcessingTaskDto.setInstitutionId(getInstitutionId());
        }
        offlineProcessingTaskDto.setUserFullName(getUserFullName());


        return offlineProcessingTaskDto;
    }
//    public String getScheduleTaskLogStatus (String statusId){
//        ScheduleTaskLogStatus status = (ScheduleTaskLogStatus) mongoRepositoryReactive.findById(statusId, ScheduleTaskLogStatus.class).block();
//        return status.getName();
//    }
}
