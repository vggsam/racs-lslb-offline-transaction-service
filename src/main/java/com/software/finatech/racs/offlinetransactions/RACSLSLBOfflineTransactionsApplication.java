package com.software.finatech.racs.offlinetransactions;

import com.software.finatech.racs.offlinetransactions.integration.JobsServiceImpl;
import com.software.finatech.racs.offlinetransactions.integration.SchedulerChannel;
import com.software.finatech.racs.offlinetransactions.util.GlobalApplicationContext;
import com.software.finatech.racs.offlinetransactions.util.MapperReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@EnableDiscoveryClient
@EnableBinding({SchedulerChannel.class})
@EnableScheduling
public class RACSLSLBOfflineTransactionsApplication {
    @Autowired
    JobsServiceImpl jobsService;
    private static Logger logger = LoggerFactory.getLogger(RACSLSLBOfflineTransactionsApplication.class);

    @LoadBalanced
    @Bean
    public RestTemplate loadRestTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(RACSLSLBOfflineTransactionsApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        ApplicationContext ctx = app.run(args);

        // Our global app context
        GlobalApplicationContext.ctx = ctx;

    }
    /*@StreamListener(target = SchedulerChannel.IN_BOUND_DATA_PENDING_COMPLETED_EVENTS)
    public void processTransaction(String transactionCreateDto) {
            logger.info("Institution Data by Client " + transactionCreateDto);
        jobsService.runPendingIds(transactionCreateDto);
    }*/
}

