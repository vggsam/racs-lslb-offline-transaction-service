package com.software.finatech.racs.offlinetransactions.dto;


import java.util.List;

public class PendingGamingIdsDto {
    protected String institutionId;
    protected List<String> gamingIds;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public List<String> getGamingIds() {
        return gamingIds;
    }

    public void setGamingIds(List<String> gamingIds) {
        this.gamingIds = gamingIds;
    }
}
