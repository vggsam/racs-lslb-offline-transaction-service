package com.software.finatech.racs.offlinetransactions.dto;


import java.util.Map;

public class DataSourceDto {

    protected String dataSourceTypeId;
    protected String description;
    protected String baseUrl;
    protected String id;
    protected String institutionId;
    protected Map<String, String> headers;
    protected Map<String, String>  params;
    protected Map<String, String>  body;
    protected String authorizationType;

    protected String pullNewGamesEndPoint;
    protected String pushNewGamesSuccessEndpoint;
    protected String pullGamesStatusChangeEndPoint;
    protected String pushGamesStatusChangeSuccessEndPoint;
    protected String loginEndpoint;


    public String getLoginEndpoint() {
        return loginEndpoint;
    }

    public void setLoginEndpoint(String loginEndpoint) {
        this.loginEndpoint = loginEndpoint;
    }

    public String getPullNewGamesEndPoint() {
        return pullNewGamesEndPoint;
    }

    public void setPullNewGamesEndPoint(String pullNewGamesEndPoint) {
        this.pullNewGamesEndPoint = pullNewGamesEndPoint;
    }

    public String getPushNewGamesSuccessEndpoint() {
        return pushNewGamesSuccessEndpoint;
    }

    public void setPushNewGamesSuccessEndpoint(String pushNewGamesSuccessEndpoint) {
        this.pushNewGamesSuccessEndpoint = pushNewGamesSuccessEndpoint;
    }

    public String getPullGamesStatusChangeEndPoint() {
        return pullGamesStatusChangeEndPoint;
    }

    public void setPullGamesStatusChangeEndPoint(String pullGamesStatusChangeEndPoint) {
        this.pullGamesStatusChangeEndPoint = pullGamesStatusChangeEndPoint;
    }

    public String getPushGamesStatusChangeSuccessEndPoint() {
        return pushGamesStatusChangeSuccessEndPoint;
    }

    public void setPushGamesStatusChangeSuccessEndPoint(String pushGamesStatusChangeSuccessEndPoint) {
        this.pushGamesStatusChangeSuccessEndPoint = pushGamesStatusChangeSuccessEndPoint;
    }

    public String getAuthorizationType() {
        return authorizationType;
    }

    public void setAuthorizationType(String authorizationType) {
        this.authorizationType = authorizationType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Map<String, String> getBody() {
        return body;
    }

    public void setBody(Map<String, String> body) {
        this.body = body;
    }

    public String getDataSourceTypeId() {
        return dataSourceTypeId;
    }

    public void setDataSourceTypeId(String dataSourceTypeId) {
        this.dataSourceTypeId = dataSourceTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }



}
