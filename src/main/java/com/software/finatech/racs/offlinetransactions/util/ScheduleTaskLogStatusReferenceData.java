package com.software.finatech.racs.offlinetransactions.util;

import com.software.finatech.racs.offlinetransactions.domain.ScheduleTaskLogStatus;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;



public class ScheduleTaskLogStatusReferenceData {
    public final static String RUNNING="1";
    public final static String FAILED="2";
    public final static String COMPLETED="3";
    public final static String AWAITING="4";
    public static void load(MongoRepositoryReactiveImpl mongoRepositoryReactive){


        ScheduleTaskLogStatus status1 = (ScheduleTaskLogStatus) mongoRepositoryReactive.findById(RUNNING,ScheduleTaskLogStatus.class).block();
        if(status1==null){
            status1 = new ScheduleTaskLogStatus();
            status1.setId(RUNNING);

        }
        status1.setDescription("RUNNING");
        status1.setName("RUNNING");

        ScheduleTaskLogStatus status2 = (ScheduleTaskLogStatus) mongoRepositoryReactive.findById(FAILED,ScheduleTaskLogStatus.class).block();
        if(status2==null){
            status2 = new ScheduleTaskLogStatus();
            status2.setId(FAILED);

        }
        status2.setDescription("FAILED");
        status2.setName("FAILED");


        ScheduleTaskLogStatus status3 = (ScheduleTaskLogStatus) mongoRepositoryReactive.findById(COMPLETED,ScheduleTaskLogStatus.class).block();
        if(status3==null){
            status3 = new ScheduleTaskLogStatus();
            status3.setId(COMPLETED);

        }
        status3.setDescription("COMPLETED");
        status3.setName("COMPLETED");


        ScheduleTaskLogStatus status4 = (ScheduleTaskLogStatus) mongoRepositoryReactive.findById(AWAITING,ScheduleTaskLogStatus.class).block();
        if(status4==null){
            status4 = new ScheduleTaskLogStatus();
            status4.setId(AWAITING);

        }
        status4.setDescription("AWAITING");
        status4.setName("AWAITING");

        mongoRepositoryReactive.saveOrUpdate(status1);
        mongoRepositoryReactive.saveOrUpdate(status2);
        mongoRepositoryReactive.saveOrUpdate(status3);
        mongoRepositoryReactive.saveOrUpdate(status4);


    }
}
