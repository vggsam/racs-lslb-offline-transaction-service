package com.software.finatech.racs.offlinetransactions.integration;

import com.software.finatech.racs.offlinetransactions.domain.HeaderMapping;
import com.software.finatech.racs.offlinetransactions.domain.OfflineProcessingTask;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;
import com.software.finatech.racs.offlinetransactions.util.ScheduleTaskLogStatusReferenceData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class Mapper {
    @Autowired
    protected MongoRepositoryReactiveImpl mongoRepositoryReactive;
    private static Logger logger = LoggerFactory.getLogger(Mapper.class);

    public HeaderMapping mapping(OfflineProcessingTask task) throws Exception {

        Query queryMapping = new Query();
        queryMapping.addCriteria(Criteria.where("institutionId").is(task.getInstitutionId()));
        queryMapping.addCriteria(Criteria.where("gameTypeId").is(task.getGameTypeId()));

        HeaderMapping headerMapping = (HeaderMapping) mongoRepositoryReactive.find(queryMapping, HeaderMapping.class).block();
        if (headerMapping == null) {
            task.setProcessed(true);
            task.setErrorMessage(" Mapping does not exist for the game type of the institution ");
            task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
            task.setStopOnError(true);
            mongoRepositoryReactive.saveOrUpdate(task);
            logger.error(" Mapping does not exist for the game type of the institution ");
            throw new Exception(" No header Mapping");
            //return null;
        }else{
            return headerMapping;
            }
        }


}
