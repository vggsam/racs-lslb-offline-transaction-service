package com.software.finatech.racs.offlinetransactions.util;

public class ColumnIndex {
    private String columnName;
    private Integer indexNumber;


    public ColumnIndex() {
    }

    public static ColumnIndex fromColumnNameAndNumber(String columnName,Integer indexNumber){
        ColumnIndex columnIndex = new ColumnIndex();
        columnIndex.setColumnName(columnName);
        columnIndex.setIndexNumber(indexNumber);
        return columnIndex;
    }


    public static ColumnIndex fromColumnName(String columnName){
        ColumnIndex columnIndex = new ColumnIndex();
        columnIndex.setColumnName(columnName);
        return columnIndex;
    }
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(Integer indexNumber) {
        this.indexNumber = indexNumber;
    }
}
