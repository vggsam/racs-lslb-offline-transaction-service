package com.software.finatech.racs.offlinetransactions.domain;

import com.software.finatech.racs.offlinetransactions.dto.ScheduleTaskLogStatusDto;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ScheduleTaskLogStatus")
public class ScheduleTaskLogStatus extends EnumeratedFact {

    public ScheduleTaskLogStatusDto convertToDto() {
        ScheduleTaskLogStatusDto scheduleTaskLogStatusDto = new ScheduleTaskLogStatusDto();
        scheduleTaskLogStatusDto.setCode(getCode());
        scheduleTaskLogStatusDto.setDescription(getDescription());
        scheduleTaskLogStatusDto.setName(getName());
        scheduleTaskLogStatusDto.setId(getId());

        return scheduleTaskLogStatusDto;
    }
}
