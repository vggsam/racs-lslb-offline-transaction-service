package com.software.finatech.racs.offlinetransactions.dto;

import java.io.Serializable;

public class TransactionEvent implements Serializable {
    protected String data;
    protected String institutionId;
    protected String gameTypeId;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(String gameTypeId) {
        this.gameTypeId = gameTypeId;
    }
}
