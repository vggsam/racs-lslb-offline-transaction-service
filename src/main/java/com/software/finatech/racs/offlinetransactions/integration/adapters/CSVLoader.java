package com.software.finatech.racs.offlinetransactions.integration.adapters;

import com.software.finatech.racs.offlinetransactions.domain.HeaderMapping;
import com.software.finatech.racs.offlinetransactions.domain.OfflineProcessingTask;
import com.software.finatech.racs.offlinetransactions.dto.OSBTransactionDto;
import com.software.finatech.racs.offlinetransactions.dto.POLTransactionDto;
import com.software.finatech.racs.offlinetransactions.dto.TransactionEvent;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;
import com.software.finatech.racs.offlinetransactions.util.ColumnIndex;
import com.software.finatech.racs.offlinetransactions.util.MapperReader;
import com.software.finatech.racs.offlinetransactions.util.ScheduleTaskLogStatusReferenceData;
import io.advantageous.boon.json.JsonFactory;
import io.advantageous.boon.json.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;


@Component("csvLoader")
public class CSVLoader {
    @Autowired
    protected MongoRepositoryReactiveImpl mongoRepositoryReactive;
    @Autowired
    MapperReader mapperReader;

    protected ObjectMapper mapper;

    @PostConstruct
    public void initialize() {
        mapper = JsonFactory.createUseAnnotations(true);
    }


    private static Logger logger = LoggerFactory.getLogger(CSVLoader.class);


    public TransactionEvent readInputStream(InputStream input, OfflineProcessingTask task, HeaderMapping headerMapper) throws Exception {
        // Read the text input stream one line at a time and display each line.
        Map<String, ColumnIndex> map = null;
        List<OSBTransactionDto> osbTransactionDtos = new ArrayList<>();
        List<POLTransactionDto> polTransactionDtos = new ArrayList<>();
        String response = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        HashSet<String>fieldValues = new HashSet<>();

        fieldValues.add(headerMapper.getGameName());
        fieldValues.add(headerMapper.getGameId());
        fieldValues.add(headerMapper.getStakedAmount());
        if(!StringUtils.isEmpty(headerMapper.getModeOfPlay())){
            fieldValues.add(headerMapper.getModeOfPlay());
        }if(!StringUtils.isEmpty(headerMapper.getTerminalId())){
            fieldValues.add(headerMapper.getTerminalId());
        }if(!StringUtils.isEmpty(headerMapper.getTransactionTime())){
            fieldValues.add(headerMapper.getTransactionTime());
        }if(!StringUtils.isEmpty(headerMapper.getTransactionDate())){
            fieldValues.add(headerMapper.getTransactionDate());
        }if(!StringUtils.isEmpty(headerMapper.getTransactionDateTime())){
            fieldValues.add(headerMapper.getTransactionDateTime());
        }if(!StringUtils.isEmpty(headerMapper.getBonus())){
            fieldValues.add(headerMapper.getBonus());
        }if(!StringUtils.isEmpty(headerMapper.getPossibleWin())){
            fieldValues.add(headerMapper.getPossibleWin());
        }if(!StringUtils.isEmpty(headerMapper.getSlipType())){
            fieldValues.add(headerMapper.getSlipType());
        }if(!StringUtils.isEmpty(headerMapper.getAgentId())){
            fieldValues.add(headerMapper.getAgentId());
        }if(!StringUtils.isEmpty(headerMapper.getDrawDateTime())){
            fieldValues.add(headerMapper.getDrawDateTime());
        }if(!StringUtils.isEmpty(headerMapper.getStatus())){
            fieldValues.add(headerMapper.getStatus());
        }if(!StringUtils.isEmpty(headerMapper.getConfirmedWin())){
            fieldValues.add(headerMapper.getConfirmedWin());
        }if(!StringUtils.isEmpty(headerMapper.getExtra1())){
            fieldValues.add(headerMapper.getExtra1());
        }if(!StringUtils.isEmpty(headerMapper.getExtra2())){
            fieldValues.add(headerMapper.getExtra2());
        }if(!StringUtils.isEmpty(headerMapper.getExtra3())){
            fieldValues.add(headerMapper.getExtra3());
        }
        boolean flag = false;
        int pos =0;
        int headerCount =0;
        while ((line = reader.readLine()) != null) {
            String copy = new String();
            boolean inQuotes = false;

            for (int j = 0; j < line.length(); ++j) {
                if (line.charAt(j) == '"')
                    inQuotes = !inQuotes;
                if (line.charAt(j) == ',' && inQuotes)
                    copy += "";
                else
                    copy += line.charAt(j);
            }
            logger.info("Copy :   " + copy);

            copy= copy.replace("\"","");

            copy= copy.replace("{","");

            copy= copy.replace("}","");

            String[] dataEntry = copy.split(headerMapper.getCsvSplitter());

            if (pos == 0) {
                for (String header: dataEntry) {
                    if(!fieldValues.contains(header.toLowerCase())){
                        task.setStopOnError(true);
                        task.setProcessed(true);
                        task.setErrorMessage("The header: "+ header+ ", is not valid");
                        task.setProcessingEndDateTime(LocalDateTime.now());
                        task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
                        mongoRepositoryReactive.saveOrUpdate(task);
                        flag = true;

                        break;
                    }
                }
                if(flag){
                    logger.error("Invalid Header mapper for "+ task.getInstitutionName()+ " with the used gameType");
                    throw new Exception("Invalid Header mapper");
                }
                headerCount = dataEntry.length;
               try {
                   map = mapperReader.getInitialMappingFromFirstLine(copy, headerMapper);
               }catch (Exception ex){
                   task.setStopOnError(true);
                   task.setProcessed(true);
                   task.setErrorMessage(ex.getMessage());
                   task.setProcessingEndDateTime(LocalDateTime.now());
                   task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
                   mongoRepositoryReactive.saveOrUpdate(task);
               }
                pos =pos + 1;
            } else {
                if(headerCount != dataEntry.length){
                    task.setStopOnError(true);
                    task.setProcessed(true);
                    task.setErrorMessage("Missing attributes in line: "+ (pos+1));
                    task.setProcessingEndDateTime(LocalDateTime.now());
                    task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
                    mongoRepositoryReactive.saveOrUpdate(task);
                    logger.error("Missing attributes in line: "+ (pos+1));
                    throw new Exception("Missing attributes in line: "+ (pos+1));
                }
                if(headerMapper.getGameTypeId().equals("01")){
                    try{
                        POLTransactionDto polTransactionFromLineAndMap = mapperReader.getPOLTransactionFromLineAndMap(copy, map, headerMapper);
                        polTransactionDtos.add(polTransactionFromLineAndMap);
                    }catch (Exception ex){
                        task.setStopOnError(true);
                        task.setProcessed(true);
                        task.setErrorMessage(ex.getMessage());
                        task.setProcessingEndDateTime(LocalDateTime.now());
                        task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
                        mongoRepositoryReactive.saveOrUpdate(task);
                    }

                }else if(headerMapper.getGameTypeId().equals("02") || headerMapper.getGameTypeId().equals("03")){
                    try{
                        OSBTransactionDto osbTransactionDto = mapperReader.getOSBTransactionFromLineAndMap(copy, map, headerMapper);
                        osbTransactionDtos.add(osbTransactionDto);
                    }catch (Exception ex){
                        task.setStopOnError(true);
                        task.setProcessed(true);
                        task.setErrorMessage(ex.getMessage());
                        task.setProcessingEndDateTime(LocalDateTime.now());
                        task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.FAILED);
                        mongoRepositoryReactive.saveOrUpdate(task);
                    }
                }else if(headerMapper.getGameTypeId().equals("04")){
                    // OSBTransactionDto osbTransactionDto = mapperReader.getOSBTransactionFromLineAndMap(copy, map, headerMapper);
                   // osbTransactionDtos.add(osbTransactionDto);
                }


            }

        }
        if(headerMapper.getGameTypeId().equals("01")){
             response = mapper.toJson(polTransactionDtos);
        }if(headerMapper.getGameTypeId().equals("02") ||
                headerMapper.getGameTypeId().equals("03")){
             response = mapper.toJson(osbTransactionDtos);
        }
        TransactionEvent transactionEvent = new TransactionEvent();

        transactionEvent.setData(response);
        transactionEvent.setGameTypeId(headerMapper.getGameTypeId());
        transactionEvent.setInstitutionId(headerMapper.getInstitutionId());
        return transactionEvent;
    }

}