package com.software.finatech.racs.offlinetransactions.domain;

import com.software.finatech.racs.offlinetransactions.dto.HeaderMappingDto;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "HeaderMappings")
public class HeaderMapping extends AbstractFact{
    private String stakedAmount;
    private String slipType;
    private String possibleWin;
    private String confirmedWin;
    private String bonus;
    private String gameId;
    private String terminalId;
    private String agentId;
    private String drawDateTime;
    private String transactionDate;
    private String transactionTime;
    private String transactionDateTime;
    private String gameName;
    private String status;
    private String modeOfPlay;
    private String gameTypeId;
    private String institutionId;
    private String transactionDateFormat;
    private String fileFormat;
    private String csvSplitter;
    private String extra1;
    private String extra2;
    private String extra3;

    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    public String getExtra3() {
        return extra3;
    }

    public void setExtra3(String extra3) {
        this.extra3 = extra3;
    }

    public String getCsvSplitter() {
        return csvSplitter;
    }

    public void setCsvSplitter(String csvSplitter) {
        this.csvSplitter = csvSplitter;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }
    public String getGameTypeId() {
        return gameTypeId;
    }
    public void setGameTypeId(String gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public String getStakedAmount() {
        return stakedAmount;
    }

    public void setStakedAmount(String stakedAmount) {
        this.stakedAmount = stakedAmount;
    }

    public String getSlipType() {
        return slipType;
    }

    public void setSlipType(String slipType) {
        this.slipType = slipType;
    }

    public String getPossibleWin() {
        return possibleWin;
    }

    public void setPossibleWin(String possibleWin) {
        this.possibleWin = possibleWin;
    }

    public String getConfirmedWin() {
        return confirmedWin;
    }

    public void setConfirmedWin(String confirmedWin) {
        this.confirmedWin = confirmedWin;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getDrawDateTime() {
        return drawDateTime;
    }

    public void setDrawDateTime(String drawDateTime) {
        this.drawDateTime = drawDateTime;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModeOfPlay() {
        return modeOfPlay;
    }

    public void setModeOfPlay(String modeOfPlay) {
        this.modeOfPlay = modeOfPlay;
    }
    public String getTransactionDateFormat() {
        return transactionDateFormat;
    }

    public void setTransactionDateFormat(String transactionDateFormat) {
        this.transactionDateFormat = transactionDateFormat;
    }

    @Override
    public String getFactName() {
        return "HeaderMappings";
    }

    public HeaderMappingDto convertToDto() {
        HeaderMappingDto headerMappingDto = new HeaderMappingDto();
        headerMappingDto.setAgentId(getId());
        headerMappingDto.setBonus(getBonus()==null ? null:getBonus());
        headerMappingDto.setConfirmedWin(getConfirmedWin()==null ? null:getConfirmedWin());
        headerMappingDto.setCsvSplitter(getCsvSplitter()==null ? null: getCsvSplitter());
        headerMappingDto.setDrawDateTime(getDrawDateTime() == null ? null: getDrawDateTime());
        headerMappingDto.setFileFormat(getFileFormat());
        headerMappingDto.setGameName(getGameName());
        headerMappingDto.setGameId(getGameId());
        headerMappingDto.setGameTypeId(getGameTypeId());
        headerMappingDto.setInstitutionId(getInstitutionId());
        headerMappingDto.setModeOfPlay(getModeOfPlay()==null ? null:getModeOfPlay());
        headerMappingDto.setStatus(getStatus()==null ? null:getStatus());
        headerMappingDto.setStakedAmount(getStakedAmount());
        headerMappingDto.setPossibleWin(getPossibleWin()==null ? null: getPossibleWin());
        headerMappingDto.setTransactionTime(getTransactionTime());
        headerMappingDto.setTransactionDate(getTransactionDate());
        headerMappingDto.setTransactionDateFormat(getTransactionDateFormat());
        headerMappingDto.setSlipType(getSlipType()==null ? null:getSlipType());
        headerMappingDto.setTerminalId(getTerminalId()==null ? null: getTerminalId());
        headerMappingDto.setExtra1(getExtra1()==null ? null: getExtra1());
        headerMappingDto.setExtra1(getExtra2()==null ? null: getExtra2());
        headerMappingDto.setExtra1(getExtra3()==null ? null: getExtra3());


        return headerMappingDto;
    }

}
