package com.software.finatech.racs.offlinetransactions.dto;


public class POLTransactionDto extends TransactionDTO {
    protected String id;
    protected String ticketId;
    protected String terminalId;
    protected String agentId;
    protected Double amountStaked;
    protected Double amountWon;
    protected String drawDateTime;

    public String getDrawDateTime() {
        return drawDateTime;
    }

    public void setDrawDateTime(String drawDateTime) {
        this.drawDateTime = drawDateTime;
    }

    @Override
    public String toString() {
        return "POLTransactionDto{" +
                "id='" + id + '\'' +
                ", ticketId='" + ticketId + '\'' +
                ", terminalId='" + terminalId + '\'' +
                ", agentId='" + agentId + '\'' +
                ", amountStaked=" + amountStaked +
                ", amountWon=" + amountWon +
                ", institutionId='" + institutionId + '\'' +
                ", transactionDate='" + transactionDate + '\'' +
                ", transactionTime='" + transactionTime + '\'' +
                ", transactionDateTime='" + transactionDateTime + '\'' +
                ", gameType='" + gameType + '\'' +
                ", amountStaked=" + amountStaked +
                ", gameName='" + gameName + '\'' +
                ", modeOfPlayId='" + modeOfPlay + '\'' +
                ", statusId='" + status + '\'' +
                ", gamingTax='" + gamingTax + '\'' +
                ", royaltyFee='" + royaltyFee + '\'' +
                ", institutionName='" + institutionName + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Double getAmountStaked() {
        return amountStaked;
    }

    public Double getAmountWon() {
        return amountWon;
    }

    public void setAmountWon(Double amountWon) {
        this.amountWon = amountWon;
    }

    public Double getGamingTax() {
        return gamingTax;
    }

    public void setGamingTax(Double gamingTax) {
        this.gamingTax = gamingTax;
    }

    public Double getRoyaltyFee() {
        return royaltyFee;
    }

    public void setRoyaltyFee(Double royaltyFee) {
        this.royaltyFee = royaltyFee;
    }
}
