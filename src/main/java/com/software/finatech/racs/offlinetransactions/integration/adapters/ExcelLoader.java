package com.software.finatech.racs.offlinetransactions.integration.adapters;

import com.software.finatech.racs.offlinetransactions.domain.HeaderMapping;
import com.software.finatech.racs.offlinetransactions.domain.OfflineProcessingTask;
import com.software.finatech.racs.offlinetransactions.dto.OSBTransactionDto;
import com.software.finatech.racs.offlinetransactions.dto.POLTransactionDto;
import com.software.finatech.racs.offlinetransactions.dto.TransactionEvent;
import com.software.finatech.racs.offlinetransactions.integration.Mapper;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

@Component
public class ExcelLoader {
    @Autowired
    protected MongoRepositoryReactiveImpl mongoRepositoryReactive;
    @Autowired
    protected MongoTemplate mongoTemplate;

    private static Logger logger = LoggerFactory.getLogger(ExcelLoader.class);

    public static TransactionEvent readInputStream(InputStream input, OfflineProcessingTask task, HeaderMapping headerMapper) throws IOException {
        Workbook workbook = WorkbookFactory.create(input);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            int pos = 0;
            while (cellIterator.hasNext()) {
                if(pos == 0){
                    if(task.getGameTypeId() == "01"){

                    }else if(task.getGameTypeId()== "02" || task.getGameTypeId()== "03"){

                    }
                }else{
                    pos =  1;
                    Cell cell = cellIterator.next();
                    if(cell.getCellType() == CellType.STRING){
                        System.out.print(cell.getStringCellValue());
                    }else if(cell.getCellType() == CellType.BOOLEAN){
                        System.out.print(cell.getBooleanCellValue());
                    }else if(cell.getCellType() == CellType.NUMERIC){
                        System.out.print(cell.getNumericCellValue());
                    }

                    System.out.print(" - ");
                }

//                if (pos == 0) {
//                    map = mapperReader.getInitialMappingFromFirstLine(copy, headerMapper);
//                    pos = 1;
//                } else {
//                    if(headerMapper.getGameTypeId() == "01"){
//                        POLTransactionDto polTransactionFromLineAndMap = mapperReader.getPOLTransactionFromLineAndMap(copy, map, headerMapper);
//                        polTransactionDtos.add(polTransactionFromLineAndMap);
//                    }else if(headerMapper.getGameTypeId() == "02"){
//                        OSBTransactionDto osbTransactionDto = mapperReader.getOSBTransactionFromLineAndMap(copy, map, headerMapper);
//                        osbTransactionDtos.add(osbTransactionDto);
//                    }else if(headerMapper.getGameTypeId() == "03"){
//                        OSBTransactionDto osbTransactionDto = mapperReader.getOSBTransactionFromLineAndMap(copy, map, headerMapper);
//                        osbTransactionDtos.add(osbTransactionDto);
//                    }
//
//                    logger.info("data entry first element => {}", dataEntry[0]);
//                }

            }
            System.out.println();
        }
        workbook.close();
        input.close();
        return null;
    }
}
