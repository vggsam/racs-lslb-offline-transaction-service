package com.software.finatech.racs.offlinetransactions.integration;

public interface JobsService {
    Boolean scheduleJobs();

}
