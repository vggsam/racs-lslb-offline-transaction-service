package com.software.finatech.racs.offlinetransactions.integration;

import com.amazonaws.services.s3.model.*;
import com.software.finatech.racs.offlinetransactions.domain.HeaderMapping;
import com.software.finatech.racs.offlinetransactions.domain.OfflineProcessingTask;
import com.software.finatech.racs.offlinetransactions.dto.TransactionEvent;
import com.software.finatech.racs.offlinetransactions.integration.adapters.CSVLoader;
import com.software.finatech.racs.offlinetransactions.integration.adapters.ExcelLoader;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;
import com.software.finatech.racs.offlinetransactions.util.FileTypeReferenceData;
import com.software.finatech.racs.offlinetransactions.util.ScheduleTaskLogStatusReferenceData;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import java.io.*;

@Component
public class S3BucketClientServiceImpl implements S3ClientService {
    private String awsS3AudioBucket;
    private AmazonS3 amazonS3;
    private static final Logger logger = LoggerFactory.getLogger(S3BucketClientServiceImpl.class);
    @Autowired
    CSVLoader csvLoader;
    @Autowired
    protected Mapper headerMap;

    @Autowired
    protected ExcelLoader excelLoader;

    @Autowired
    protected MongoRepositoryReactiveImpl mongoRepositoryReactive;

    @Autowired
    protected SchedulerChannel schedulerChannel;

    @Autowired
    public S3BucketClientServiceImpl(Region awsRegion, AWSCredentialsProvider awsCredentialsProvider, String awsS3AudioBucket) {
        this.amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(awsRegion.getName()).build();
        this.awsS3AudioBucket = awsS3AudioBucket;
    }



    @Async
    public void deleteFileFromS3Bucket(String fileName) {
        try {
            amazonS3.deleteObject(new DeleteObjectRequest(awsS3AudioBucket, fileName));
        } catch (AmazonServiceException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while removing [" + fileName + "] ");
        }
    }
    @Async
    public void readFileFromS3Bucket(String s3BucketName, OfflineProcessingTask task) {
        TransactionEvent transactionEvent = null;
        GetObjectRequest getObjectRequest = new GetObjectRequest(awsS3AudioBucket, s3BucketName);
        try {
            S3Object s3Object = amazonS3.getObject(getObjectRequest);
            HeaderMapping map = headerMap.mapping(task);
            if(map.getFileFormat().equals(FileTypeReferenceData.CSV) || map.getFileFormat().equals(FileTypeReferenceData.TXT)){
               transactionEvent = csvLoader.readInputStream(s3Object.getObjectContent(), task, (HeaderMapping) map);
            }
            if(map.getFileFormat().equals(FileTypeReferenceData.EXCEL)){
                transactionEvent = excelLoader.readInputStream(s3Object.getObjectContent(), task, (HeaderMapping) map);
            }
            if( transactionEvent !=  null){
                boolean success = schedulerChannel.pullNewGames().send(MessageBuilder.withPayload(transactionEvent).build());
                if (success == true) {
                    task.setProcessingEndDateTime(LocalDateTime.now());
                    task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.COMPLETED);
                    task.setStopOnError(false);
                    logger.info("Sending "+ task.getOriginalFileName() + " Transaction Service was Successful");

                }else{
                    task.setProcessingEndDateTime(LocalDateTime.now());
                    task.setProcessed(false);
                    task.setErrorMessage("Sending "+ task.getOriginalFileName() + " to Transaction Service was Failed");
                    task.setScheduleTaskLogStatusId(ScheduleTaskLogStatusReferenceData.AWAITING);
                    task.setStopOnError(true);
                    logger.info("Sending "+ task.getOriginalFileName() + " to Transaction Service was Failed");
                }
                mongoRepositoryReactive.saveOrUpdate(task);

            }

        } catch (AmazonServiceException | IOException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while removing [" + s3BucketName + "] ");

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

}

