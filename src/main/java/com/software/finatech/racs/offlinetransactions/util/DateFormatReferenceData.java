package com.software.finatech.racs.offlinetransactions.util;


import com.software.finatech.racs.offlinetransactions.domain.DateFormat;
import com.software.finatech.racs.offlinetransactions.persistence.MongoRepositoryReactiveImpl;

public class DateFormatReferenceData {
    public final static String TYPE1="dd/MM/yyyy";
    public final static String TYPE2="dd-MM-yyyy";
    public final static String TYPE3="MM-dd-yyyy";
    public final static String TYPE4="MM/dd/yyyy";
    public final static String TYPE5="yyyy-MM-dd";
    public final static String TYPE6="yyyy/MM/dd";
    public static void load(MongoRepositoryReactiveImpl mongoRepositoryReactive){


        DateFormat date1 = (DateFormat) mongoRepositoryReactive.findById("TYPE1", DateFormat.class).block();
        if(date1==null){
            date1 = new DateFormat();
            date1.setId("TYPE1");
        }
        date1.setDescription("TYPE1");
        date1.setName(TYPE1);

        DateFormat date2 = (DateFormat) mongoRepositoryReactive.findById("TYPE2", DateFormat.class).block();
        if(date2==null){
            date2 = new DateFormat();
            date2.setId("TYPE2");
        }
        date2.setDescription("TYPE2");
        date2.setName(TYPE2);


        DateFormat date3 = (DateFormat) mongoRepositoryReactive.findById("TYPE3", DateFormat.class).block();
        if(date3==null){
            date3 = new DateFormat();
            date3.setId("TYPE3");
        }
        date3.setDescription("TYPE3");
        date3.setName(TYPE3);


        DateFormat date4 = (DateFormat) mongoRepositoryReactive.findById("TYPE4", DateFormat.class).block();
        if(date4==null){
            date4 = new DateFormat();
            date4.setId("TYPE4");

        }
        date4.setDescription("TYPE4");
        date4.setName(TYPE4);

        DateFormat date5 = (DateFormat) mongoRepositoryReactive.findById("TYPE5", DateFormat.class).block();
        if(date5==null){
            date5 = new DateFormat();
            date5.setId("TYPE5");

        }
        date5.setDescription("TYPE5");
        date5.setName(TYPE5);

        DateFormat date6 = (DateFormat) mongoRepositoryReactive.findById("TYPE6", DateFormat.class).block();
        if(date6==null){
            date6 = new DateFormat();
            date6.setId("TYPE6");

        }
        date6.setDescription("TYPE6");
        date6.setName(TYPE6);


        mongoRepositoryReactive.saveOrUpdate(date1);
        mongoRepositoryReactive.saveOrUpdate(date2);
        mongoRepositoryReactive.saveOrUpdate(date3);
        mongoRepositoryReactive.saveOrUpdate(date4);
        mongoRepositoryReactive.saveOrUpdate(date5);
        mongoRepositoryReactive.saveOrUpdate(date6);


    }
}
