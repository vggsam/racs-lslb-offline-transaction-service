package com.software.finatech.racs.offlinetransactions.dto;

import java.util.HashMap;

public class HeaderMappingDto {
    protected String id;
    protected String stakedAmount;
    protected String slipType;
    protected String possibleWin;
    protected String confirmedWin;
    protected String bonus;
    protected String gameId;
    protected String terminalId;
    protected String agentId;
    protected String drawDateTime;
    protected String transactionDate;
    protected String transactionTime;
    protected String transactionDateTime;
    protected String gameName;
    protected String status;
    protected String modeOfPlay;
    protected String gameTypeId;
    protected String institutionId;
    protected String transactionDateFormat;
    protected String fileFormat;
    protected String csvSplitter;
    protected String extra1;
    protected String extra2;
    protected String extra3;


    public String getExtra1() {
        return extra1;
    }

    public void setExtra1(String extra1) {
        this.extra1 = extra1;
    }

    public String getExtra2() {
        return extra2;
    }

    public void setExtra2(String extra2) {
        this.extra2 = extra2;
    }

    public String getExtra3() {
        return extra3;
    }

    public void setExtra3(String extra3) {
        this.extra3 = extra3;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStakedAmount() {
        return stakedAmount;
    }

    public void setStakedAmount(String stakedAmount) {
        this.stakedAmount = stakedAmount;
    }

    public String getSlipType() {
        return slipType;
    }

    public void setSlipType(String slipType) {
        this.slipType = slipType;
    }

    public String getPossibleWin() {
        return possibleWin;
    }

    public void setPossibleWin(String possibleWin) {
        this.possibleWin = possibleWin;
    }

    public String getConfirmedWin() {
        return confirmedWin;
    }

    public void setConfirmedWin(String confirmedWin) {
        this.confirmedWin = confirmedWin;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getDrawDateTime() {
        return drawDateTime;
    }

    public void setDrawDateTime(String drawDateTime) {
        this.drawDateTime = drawDateTime;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModeOfPlay() {
        return modeOfPlay;
    }

    public void setModeOfPlay(String modeOfPlay) {
        this.modeOfPlay = modeOfPlay;
    }

    public String getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(String gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getTransactionDateFormat() {
        return transactionDateFormat;
    }

    public void setTransactionDateFormat(String transactionDateFormat) {
        this.transactionDateFormat = transactionDateFormat;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public String getCsvSplitter() {
        return csvSplitter;
    }

    public void setCsvSplitter(String csvSplitter) {
        this.csvSplitter = csvSplitter;
    }


}
